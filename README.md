GOAT Angling Adventures believe you deserve to work with an experienced angler and fishing charter in Kawartha Lakes. Whether you are a novice or an experienced angler, when you put your trust in GOAT Angling Adventures you will learn how to fish, where to find fish, and how to safely handle it.

Address: 188 Charlore Park Drive, Omemee, ON K0L 2W0, Canada

Phone: 705-934-1450

Website: http://www.goatanglingadventures.ca
